---
title: Community Contributions Guide
keywords: contribution
last_updated: April 28, 2020
tags: [hpc]
sidebar: hpc_sidebar
permalink: contributing
folder: hpc
---

## Overview

This site is generated using <a href="https://jekyllrb.com/" target="_blank">Jekyll</a> and was forked from the <a href="https://github.com/tomjoht/documentation-theme-jekyll" target="_blank">Jekyll Documentation Theme</a>. It is hosted on <a href="https://gitlab.com/ucb-rit/rit-docs" target="_blank">GitLab</a> using <a href="https://about.gitlab.com/product/pages/" target="_blank">GitLab Pages</a>.

We welcome your help with to make these docs as accurate as possible. You can [submit an issue](https://gitlab.com/ucb-rit/rit-docs/-/issues) to our GitLab repository or even [fork it](https://gitlab.com/ucb-rit/rit-docs/-/forks), make your changes, and submit a [merge request](https://gitlab.com/ucb-rit/rit-docs/-/merge_requests).

Below you will find a guide for contributions to this site. This guide is consistently updated, so please check back here regularly for changes.

## 3 Workflow Options

**Important Note**: Gitlab allows you to fork _either_ `rit-docs` _or_ `rit-docs-testing`, but not both because `rit-docs-testing` is a fork of `rit-docs`. One way or another, you will be able to submit merge requests to `rit-docs:master`. If you've forked `rit-docs-testing` be sure to submit merge requests from the `master` branch and _don't let the config values from the `develop` branch get into `master`_. Decide which workflow below is best for you before deciding which repo to fork.

### Gitlab GUI Workflow

1. Open https://gitlab.com/ucb-rit/rit-docs and click the **[Fork](https://gitlab.com/ucb-rit/rit-docs/-/forks/new)** button in the upper right.
2. In your fork, navigate to the file you want to edit and click the **Edit** button or use the **Web IDE** near the top of the file viewer box.
3. Make your changes and use the **Commit** button below the file editor (or the bottom of the file navigation in the case of the Web IDE) to finalize.
4. You should be returned to file's preview page with a message that says "Your changes have been successfully committed." Below that message and to the right there should now be a **Create merge request** button. Click on that button to begin a merge request from your fork to `rit-docs:master`.
5. On the merge request page, be sure to check at the top that you are merging into `ucb-rit/rit-docs:master`. If not, click "Change branches" and select `ucb-rit/rit-docs:master` as the target branch.
6. Add a descriptive title to your merge request along with any description that can help us in reviewing your changes. When you're ready, click the **Submit merge request** button to finalize.
7. (For RIT members only) If you don't need feedback on your changes, go to [Merge Requests](https://gitlab.com/ucb-rit/rit-docs/-/merge_requests) and finish merging your changes into `ucb-rit/rit-docs:master`. Your changes will appear on the live site shortly.

### Local Workflow

1. Fork `rit-docs` as in the **Gitlab GUI Workflow**.
2. Clone your fork to your local machine.
3. Follow the steps below in [Setting up a local copy of this site](#setting-up-a-local-copy-of-this-site)
4. Make, test, and commit your local changes and push up to your fork.
5. On the Gitlab page for your fork, there should now be a **Create merge request** button as in step 4 of the **Gitlab GUI Workflow**. Continue from that step to create and finalize your merge request into `ucb-rit/rit-docs:master`.

### Advanced: Local workflow + Gitlab Pages Testing Deployment

The `ucb-rit/rit-docs-testing` fork is set up to work as a testing deployment using Gitlab Pages. This can be useful when you want to test your changes in a production-like environment or easily share them for others to review.

1. Fork `ucb-rit/rit-docs-testing`.
2. In the sidebar nav of your fork, go to **CI/CD** > **Pipelines** and click on the **Run Pipeline** button. Run for the `deploy` branch.
3. Once the pipeline has completed, in the sidebar nav of your fork, go to **Settings** > **Pages**. You should now see a gitlab.io URL that you can use to access a live version of your fork.
4. Make and test your changes [locally](#setting-up-a-local-copy-of-this-site), and read the section on [Bigger changes](#bigger-changes) below to understand the proper way to deploy your changes to the live testing site.
4. Proceed from step 5 of the **Local Workflow** above to submit a merge request to `ucb-rit/rit-docs:master`.

## Setting up a local copy of this site

You'll often want to work on changes locally before committing them to the `rit-docs-testing` site (see [Gitlab Branching / Merging Workflow](#gitlab-branching--merging-workflow)).

To set get this Jekyll site running locally, you'll have to get `gem` if you don't already have it and then install the gems `bundler` and `jekyll` via 
```
gem install bundler jekyll
```
`gem` comes with Ruby, and there are various ways of getting it but most recommend using a package manager such as homebrew on macOS. For macOS, see here: <a href="https://stackoverflow.com/questions/39381360/how-do-i-install-ruby-gems-on-mac" target="_blank">https://stackoverflow.com/questions/39381360/how-do-i-install-ruby-gems-on-mac</a>

Once you have `gem` and install `bundler` and `jekyll`, you'll need a local copy of the `rit-docs-testing` repo. 
```
git clone git@gitlab.com:ucb-rit/rit-docs-testing.git
```
`cd` into the root of the repo and run
```
bundle update
bundle exec jekyll serve --baseurl=''
```
The `--baseurl=''` is important as it ensure that the internal URLs are formed correctly when running locally. Point your browser to the "Server address".

## Adding Pages, Subdirectories and Posts

### Pages

To create a new page, add a new `.md` file somewhere in the `pages` directory. While the directory structure has no effect on URLs, it is good practice to keep the pages organized in subdirectories that correspond to the permalink structure and to name files so that they match the final part of the URL slug.

The page title, summary, tags, and everything below the front matter (see below) will make up the visible content of the page. Any `<h*>` tags that are used (e.g. the markdown for an `<h2>` tag is `##`) will become part of the Table of Contents at the top of the page below the title.

<a href="https://daringfireball.net/projects/markdown/" target="_blank">See here</a> for more info on writing markdown.

### Subdirectory Landing Pages

Subdirectory landing pages are special pages that have a landing page and serve as the base for additional pages. For example, `services/high-performance-computing/` is a subdirectory landing page which exists in the repos `pages` directory and contains a number of other pages. The `index.md` (or optionsally `index.html`) file in the `high-performance-computing` directory plays a special role as the landing page at the url `{{ site.url }}/rit-docs/services/high-performance-computing/`. If this file is not present, then navigating to that URL gives a 404 not found error even if there is a file in the directory with the permalink `services/high-performance-computing`.

**Important Note:** The `permalink` in the front matter (see below) of the `index.md` subdirectory landing page file must contain a trailing `/` or the landing page will not be found due to issues in the interaction between how GitLab Pages and Jekyll handle permalinks. For example, `permalink: services/high-performance-computing/`.

Also note that the `/services/high-performance-computing/status-and-announcements/` page is a somewhat strange case. Its permalink requires the trailing '/' even though it is not a subdirectory landing page in the sense defined above. However, as it is a "newsfeed" page, it serves as the base for post detail pages (e.g., `services/high-performance-computing/status-and-announcements/savio-back`).

### Front Matter

All pages and posts require a preamble. For example, the front matter for this page is

```
---
title: How to use this site
sidebar: hpc_sidebar
permalink: index.html
tags: [getting_started, creating_content]
keywords: hpc
summary: A brief introduction to using this HPC documentation site.
search: exclude
---
```
Note that the homepage gets the special permalink `index.html`, while other pages have the permalinks you would expect. For example, the permalink for [Getting an Account]({{ site.baseurl }}/services/high-performance-computing/getting-account) is `services/high-performance-computing/getting-account`.

Another difference is that this page is not included in search results.

**Required elements**:
* `title` - this will appear in a `<h1>` tag at the top of the page/post and is used by the "search..." field in the header of the site.
* `sidebar` - use `hpc_sidebar`.
* `permalink` - this determines the URL slug (everything after the first `/`). For pages, the final part should match the name of the file (excluding `.md`). It is also used by search.

**Optional elements**:
* `tags` - Appear at the bottom of pages and posts (see below). Also, used by search and in invisible `<meta>` tags which are useful for SEO purposes. Tags should be in brackets and comma-separated as in the example above.
* `keywords` - Used by search and in `<meta>` tags.
* `summary` - Appears beneath the title (as above) and used by search.
* `search` - Use `exclude` if you do not want the page/post to appear in search results.

For more info on the front matter, <a href="https://jekyllrb.com/docs/front-matter/" target="_blank">see here</a>.

### Links within page content

For external links, please use the following HTML snippet so that they open in a new browser tab:

```
<a href="https://your-link.com" target="_blank">This link opens in a new tab.</a>
```

For internal links, `.md` is fine but we need to use a bit of <a href="https://github.com/Shopify/liquid/wiki" target="_blank">Liquid</a> to ensure that the link works the same way in all environments (i.e. local, testing and production). For example, the link above to the Getting an Account page was created using:
```
[Getting an Account]({{ "{{ site.baseurl " }}}}/services/high-performance-computing/getting-account)
```
The key here is the small bit of Liquid templating `{{ "{{ site.baseurl " }}}}` which prepends the correct URL for a given environment.

**Warning**: By default, [`kramdown`](https://kramdown.gettalong.org/), the Markdown parser used by Jekyll, will not parse Markdown within `html` block elements. If desired, we can change this behavior by setting the following in `_config.yml` ([source](https://stackoverflow.com/questions/50069585/internal-links-not-working-for-me-in-jekyll), not tested):

```
kramdown:
  parse_block_html: true
```

### Adding Images

To add an image to the site, first upload it to the `images` folder in the root of the repo, in a subfolder if desired for organization. Keep in mind that images should be as small and low-quality as possible wihtout sacrificing fidelity on a typical monitor/phone. If your image is `my-img.jpg`, you can add it to a page via Markdown or HTML:

**Markdown:**

`![Alt text]({{ "{{ site.baseurl " }}}}/images/my-img.png)`

**HTML:**

`<img alt="Alt text" height="50" width="50" src="{{ "{{ site.baseurl " }}}}/images/my-img.jpg">`

Screen readers use the `alt` attribute (in Markdown, the text in `[]`) so including it is important for accessbility.

## Gitlab Branching / Merging Workflow

**Notation**: `rit-docs:master` is the `master` branch of the `rit-docs` repository.

### <a href="https://gitlab.com/ucb-rit/rit-docs" target="_blank">`rit-docs`</a> repository

This is where the actually live user-facing documentation site lives, so you should not make changes directly to this repository (other than via merge request from the `master` branch of `rit-docs-testing`. This repo has one branch, `master` which renders at <a href="https://ucb-rit.gitlab.io/rit-docs/" target="_blank">`https://ucb-rit.gitlab.io/rit-docs/`</a>.

### <a href="https://gitlab.com/ucb-rit/rit-docs-testing" target="_blank">`rit-docs-testing`</a> repository

All changes to the documentation site should start in the <a href="https://gitlab.com/ucb-rit/rit-docs-testing" target="_blank">`rit-docs-testing`</a> repository. This repo has two main branches:

* <a href="https://gitlab.com/ucb-rit/rit-docs-testing/tree/master" target="_blank">`rit-docs-testing:master`</a>: The `master` branch should always be identical to (or, for brief moments before merging, at most one commit ahead of) the `master` of the live site's repo <a href="https://gitlab.com/ucb-rit/rit-docs" target="_blank">`rit-docs`</a>. This branch is not actually rendered at <a href="https://ucb-rit.gitlab.io/rit-docs-testing/" target="_blank">`https://ucb-rit.gitlab.io/rit-docs-testing/`</a>, and any changes to it should be made with the intention of immediately merging into `rit-docs:master`.
* <a href="https://gitlab.com/ucb-rit/rit-docs-testing/tree/deploy" target="_blank">`rit-docs-testing:deploy`</a>: The `deploy` branch is the branch that is actually rendered at <a href="https://ucb-rit.gitlab.io/rit-docs-testing/" target="_blank">`https://ucb-rit.gitlab.io/rit-docs-testing/`</a>. It's main job is to hold the correct config values for the testing site, and as a testing environment for your bigger changes (see below).

### Small changes

If you have the proper permissions and only need to change a small amount of copy or make other changes that don't have a large visual impact to the site, you can edit the <a href="https://gitlab.com/ucb-rit/rit-docs/tree/master" target="_blank">`rit-docs:master`</a> branch directly. There are two options:

1. Edit the page via the Gitlab GUI. Be sure that you are on the correct branch (`master`) and please include a commit message briefly describing the change. After you make your edits, clicking the "Commit changes" button will open a new merge request. **More info**: <a href="https://docs.gitlab.com/ee/user/project/repository/web_editor.html" target="_blank">https://docs.gitlab.com/ee/user/project/repository/web_editor.html</a> and <a href="https://docs.gitlab.com/ee/user/project/web_ide/" target="_blank">https://docs.gitlab.com/ee/user/project/web_ide/</a>.
2. (Familiarity with `git` required) Clone the `rit-docs` repo locally, make changes to the `master` branch, and push directly to `rit-docs:master**.

### Bigger changes

**Note:** Be sure to read steps 1-3 of the [Gitlab Pages Testing Deployment](#advanced-local-workflow--gitlab-pages-testing-deployment) instructions above to get a live testing site set up using Gitlab Pages.

If you have a larger number of changes or changes that you want to test in a production-like environment, you'll want to fork `rit-docs-testing` and create a new branch off of your forked `rit-docs-testing:master`. There are probably ways to do this via the Gitlab GUI, but in the examples below I assume you've cloned your fork of `rit-docs-testing` to your local system and have both the `master` and the `deploy` branches. A possible workflow is as follows:

* Create a new branch off of `master`, e.g.
```
git clone git@gitlab.com:your-username/rit-docs-testing.git
git checkout master
git checkout -b big-changes
```
* Make your changes and merge them into the `deploy` branch locally:
```
git checkout deploy
git merge big-changes
```
* Push the local version of `deploy` to the remote `your-username/rit-docs-testing:deploy`:
```
git push origin deploy
```
* At this point the Gitlab CI/CD pipeline will run to render your changes on the live testing site. In the sidebar nav of your fork, go to **CI/CD** > **Pipelines** to see the status. Once the pipeline runs, go to your gitlab.io URL for your fork to check that your changes look good.
* If everything looked good, update your local copy of `rit-docs-testing:master`, merge your local `big-changes` into `master`, and push to the remote `master` branch:
**WARNING:** DO NOT merge `deploy` into `master`. It has special config values that will break the live production site if merged into `ucb-rit/rit-docs:master`.
```
git checkout master
git pull origin master
git merge big-changes
git push origin master
```
* Create a merge request from your fork of `rit-docs-testing:master` to `rit-docs:master` (see step 5 of the [Gitlab Pages Testing Deployment](#advanced-local-workflow--gitlab-pages-testing-deployment) above).

