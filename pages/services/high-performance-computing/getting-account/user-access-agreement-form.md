---
title: User Access Agreement Form
keywords: 
last_updated: March 10, 2020
tags: [hpc, accounts]
sidebar: hpc_sidebar
permalink: services/high-performance-computing/getting-account/user-access-agreement-form
folder: hpc
# DO NOT EDIT THE PREAMBLE BELOW THIS LINE #
search_content: Every prospective user of the Savio high performance computing cluster and other clusters managed by Berkeley Research Computing  BRC  must first complete and  electronically  sign a BRC User Access Agreement form   in order to obtain credentials to access the cluster   If you have a UC Berkeley CalNet ID   please complete this form    Note  if you have a berkeley edu email address  this is the form you should complete  Also  if you encounter a  You need permission  error  try opening the form in an Incognito   Private window in your browser   If you do not have a CalNet ID   please complete this form    Note  Do NOT fill this second form if you have a berkeley edu email address  Fill this second form ONLY if you do not have a CalNET ID or else your entry will not be valid   
---

Every prospective user of the Savio high-performance computing cluster and other clusters managed by Berkeley Research Computing (BRC) must first complete and (electronically) sign a <strong>BRC User Access Agreement form</strong>, in order to obtain credentials to access the cluster.
<div>&nbsp;</div>
<ul><li><strong>If you have a UC Berkeley CalNet ID</strong>, please complete <a data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=https://docs.google.com/a/berkeley.edu/forms/d/e/1FAIpQLSePVig8h1WZmPaUddZycygJdsBUKqvJuiTOdstQiWGDuY8qDA/viewform&amp;source=gmail&amp;ust=1478118128411000&amp;usg=AFQjCNEge-w-xzoV9Yve3WK2hF9b-QLhjw" href="https://docs.google.com/a/berkeley.edu/forms/d/e/1FAIpQLSePVig8h1WZmPaUddZycygJdsBUKqvJuiTOdstQiWGDuY8qDA/viewform" target="_blank">this form</a>.<br><br>(Note: if you have a <code>berkeley.edu</code> email address, this is the form you should complete. Also, if you encounter a "You need permission" error, try opening the form in an Incognito / Private window in your browser.)<br>&nbsp;</li>
<li><strong>If you do not have a CalNet ID</strong>, please complete <a data-saferedirecturl="https://www.google.com/url?hl=en&amp;q=https://docs.google.com/a/berkeley.edu/forms/d/e/1FAIpQLScKwiXeRwkCmWl9mFu1RvCvWVklv6Acqx7xkCvJpXogQMfU_A/viewform&amp;source=gmail&amp;ust=1478118128411000&amp;usg=AFQjCNEYYqkb99qMdubadCaNg5UvFt3mSQ" href="https://docs.google.com/a/berkeley.edu/forms/d/e/1FAIpQLScKwiXeRwkCmWl9mFu1RvCvWVklv6Acqx7xkCvJpXogQMfU_A/viewform" target="_blank">this form</a>.<br><br>(Note: Do NOT&nbsp;fill this second form if&nbsp;you have a&nbsp;<code style="font-size: 16px;">berkeley.edu</code>&nbsp;email address. Fill this second form ONLY if you do not have a CalNET ID or else your entry will not be valid.)&nbsp;</li>
</ul><p>&nbsp;</p>
<p>&nbsp;</p>
