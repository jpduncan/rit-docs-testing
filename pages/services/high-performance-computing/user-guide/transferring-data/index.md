---
title: Transferring Data
keywords: high performance computing, berkeley research computing
tags: [hpc, cloud, aws, gcp, azure]
sidebar: hpc_sidebar
permalink: services/high-performance-computing/user-guide/transferring-data/

# DO NOT EDIT THE PREAMBLE BELOW THIS LINE #
search_content: This is an overview of how to transfer data to or from the Berkeley Research Computing  BRC  supercluster  consisting of the Savio and Vector high performance computing clusters  at the University of California  Berkeley  When transferring data using file transfer software  you should connect only to the supercluster s Data Transfer Node  dtn brc berkeley edu    Note  if you re using Globus Connect  you ll instead connect to the Globus endpoint ucb brc   After connecting to the Data Transfer Node  you can transfer files directly into  and or copy files directly from  your Home directory  Group directory  if applicable  and Scratch directory  For information on making your files accessible to other users  in particular members of your group  see these instructions   Medium  to large sized data transfers When transferring a large number of files and or large files  we recommend you use  Globus Connect  formerly Globus Online    This method allows you to make unattended transfers which are fast and reliable  For basic instructions  see Using Globus Connect   You can additionally use GridFTP or BBCP for this purpose   Small sized data transfers When transferring a modest number of smaller sized files  you can also use  SFTP   For basic instructions  see Using SFTP via FileZilla   SCP   For basic instructions  see Using SCP   You can additionally use protocols like FTPS and tools like Rsync for this purpose   Transfers to from repositories under version control When your code and or data are stored in repositories under version control  client software is available for accessing them via  Git Mercurial Subversion  SVN  See Accessing Software for information on finding and loading this software via the BRC supercluster s Environment Modules  Transfers to from specific systems For bDrive  Google Drive  and Box   we recommend using rclone to transfer data to and from Savio  See here for instructions for using Box and bDrive with Savio  See here for additional information about bDrive  Google Drive  provided by our Research Data Management program  Additional tutorials for transferring files to from Amazon Web Services  AWS  S3 and other popular data storage systems are in planning or development  If you have any interest in working on or testing one of these  or have suggestions for other data transfer tutorials  please contact us via our Getting Help email address 
---

<p>This is an overview of how to transfer data to or from the Berkeley Research Computing (BRC) supercluster, consisting of the Savio and Vector high-performance computing clusters, at the University of California, Berkeley. </p>
<!--break--><p>
When transferring data using file transfer software, you should connect only to the supercluster's Data Transfer Node, <code>dtn.brc.berkeley.edu</code>. (Note: if you're using Globus Connect, you'll instead connect to the Globus endpoint <code>ucb#brc</code>)</p>
<p>After connecting to the Data Transfer Node, you can transfer files directly into (and/or copy files directly from) your Home directory, Group directory (if applicable), and Scratch directory.</p>
<p>For information on making your files accessible to other users (in particular members of your group), see <a href="{{ site.baseurl }}/services/high-performance-computing/user-guide/transferring-data/making-files-accessible">these instructions</a>.</p>
<h4>Medium- to large-sized data transfers</h4>
<p>When transferring a large number of files and/or large files, we recommend you use:</p>
<ul><li><strong>Globus Connect (formerly Globus Online)</strong>: This method allows you to make unattended transfers which are fast and reliable. For basic instructions, see <a href="{{ site.baseurl }}/services/high-performance-computing/user-guide/transferring-data/using-globus-connect-savio">Using Globus Connect</a>.</li>
</ul><p>You can additionally use <a href="https://en.wikipedia.org/wiki/GridFTP" target="_blank">GridFTP</a> or <a href="https://www.slac.stanford.edu/~abh/bbcp/" target="_blank">BBCP</a> for this purpose ...</p>
<h4>Small-sized data transfers</h4>
<p>When transferring a modest number of smaller-sized files, you can also use:</p>
<ul><li><strong>SFTP</strong>: For basic instructions, see <a href="{{ site.baseurl }}/services/high-performance-computing/user-guide/transferring-data/using-sftp-savio-filezilla">Using SFTP via FileZilla</a>.</li>
<li><strong>SCP</strong>: For basic instructions, see <a href="{{ site.baseurl }}/services/high-performance-computing/user-guide/transferring-data/using-scp-savio">Using SCP</a>.</li>
</ul><p>You can additionally use protocols like <a href="https://en.wikipedia.org/wiki/FTPS" target="_blank">FTPS</a> and tools like <a href="https://en.wikipedia.org/wiki/Rsync" target="_blank">Rsync</a> for this purpose ...</p>
<h4>Transfers to/from repositories under version control</h4>
<p>When your code and/or data are stored in repositories under version control, client software is available for accessing them via:</p>
<ul><li><strong>Git</strong></li>
<li><strong>Mercurial</strong></li>
<li><strong>Subversion (SVN)</strong></li>
</ul><p>See <a href="{{ site.baseurl }}/services/high-performance-computing/user-guide/accessing-software">Accessing Software</a> for information on finding and loading this software via the BRC supercluster's Environment Modules.</p>
<h4>Transfers to/from specific systems</h4>
<p>For <strong>bDrive (Google Drive)</strong>&nbsp;and <strong>Box</strong>, we recommend using rclone to transfer data to and from Savio.&nbsp;</p>
<ul>
<li>See <a href="{{ site.baseurl }}/services/high-performance-computing/user-guide/transferring-data/rclone-box-bdrive">here</a> for instructions for using Box and bDrive with Savio.</li>
<li>See <a href="http://research-it.berkeley.edu/services/research-data-management-service/take-advantage-unlimited-bdrive-storage-using-rclone" target="_blank">here</a> for additional information about bDrive (Google Drive) provided by our Research Data Management program.</li>
</ul><p>Additional tutorials for transferring files to/from Amazon Web Services (AWS) S3 and other popular data storage systems are in planning or development. If you have any interest in working on or testing one of these, or have suggestions for other data transfer tutorials, please contact us via our <a href="{{ site.baseurl }}/services/high-performance-computing/getting-help">Getting Help</a> email address!</p>
