---
title: GNU parallel
keywords: high performance computing
last_updated: May 12, 2020
tags: [hpc]
sidebar: hpc_sidebar
permalink: services/high-performance-computing/user-guide/running-your-jobs/gnu-parallel
folder: hpc
# DO NOT EDIT THE PREAMBLE BELOW THIS LINE #
search_content: GNU Parallel is a shell tool for executing jobs in parallel on one or multiple computers  It s a helpful tool for automating the parallelization of multiple  often serial  jobs  in particular allowing one to group jobs into a single SLURM submission to take advantage of the multiple cores on a given Savio node   A job can be a single core serial task  multi core job  or MPI application  A job can also be a command that reads from a pipe  The typical input is a list of input parameters needed as input for all the jobs collectively  GNU parallel can then split the input and pipe it into commands in parallel  GNU parallel makes sure output from the commands is the same output as you would get had you run the commands sequentially  and output names can be easily tied to input file names for simple post processing  This makes it possible to use output from GNU parallel as input for other programs   Below we ll show basic usage of GNU parallel and then provide an extended example illustrating submission of a Savio job that uses GNU parallel   For full documentation see the  GNU parallel man page https www gnu org software parallel man html  and  GNU parallel tutorial https www gnu org software parallel parallel tutorial html     Basic usage  To motivate usage of GNU parallel  consider how you might automate running multiple individual tasks using a simple bash for loop  In this case  our example command involves copying a file  We will copy  file1 in  to  file1 out   file2 in  to  file2 out  etc     for   i 1  i     3  i    do cp file i in file i out done    That s fine  but it won t run the tasks in parallel  Let s use GNU parallel to do it in parallel     parallel  j 2 cp file in file out   1 2 3 ls file out   file1 out  file2 out  file3 out    Based on  j  that will use two cores to process the three tasks  starting the third task when a core becomes free from having finished either the first or second task  The   syntax separates the input values   1 2 3  from the command being run  Each input value is used in place of   and the  cp  command is run     Some bells and whistles  We can use multiple inputs per task  distinguishing the inputs by  1   2  etc     parallel  link  j 2 cp file 1 in file 2 out   1 2 3   4 5 6 ls file out   file4 out  file5 out  file6 out    Note that  link  is needed so that 1 is paired with 4  2 with 5  etc  instead of doing all possible pairs amongst the two sets   Of course in many contexts we don t want to have to write out all the input numbers  We can instead generate them using  seq     parallel  j 2 cp file in file out    seq 3     We can use a file containing the list of inputs as a  task list  instead of using the   syntax  Here we ll also illustrate the special syntax   to remove the filename extension     parallel  j 2  a task lst cp    out     task lst  looks like this  it should have the parameter s  for separate tasks on separate lines     file1 in file2 in file3 in    Next we could use a shell script instead of putting the command inline     parallel  j 2  a task lst bash mycp sh    out   copying file1 in to file1 out   copying file2 in to file2 out   copying file3 in to file3 out    Here s what  mycp sh  looks like      bin bash echo copying  1  to  2  cp  1   2     We could also parallelize an arbitrary set of commands rather than using the same command on a set of arbitrary inputs     parallel  j 2   commands lst   hello   wilkommen   hola    Not surprisingly  here s the content of  commands lst     echo hello echo wilkommen echo hola    Finally  let s see how we would use GNU parallel within the context of a SLURM batch job   To parallelize on one node  using all the cores on the node     module load gnu parallel 2019 03 22 parallel  j  SLURM CPUS ON NODE   commands lst    To parallelize across all the cores on multiple nodes we need to use the  slf  flag     module load gnu parallel 2019 03 22 echo  SLURM JOB NODELIST  sed s n g   hostfile parallel  j  SLURM CPUS ON NODE  slf hostfile   commands lst    When using multiple nodes  the working directory will be your home directory  unless you specify otherwise using the  wd  flag  see below for example usage  and NOT the directory from which  parallel  was called     Extended example  Here we ll put it all together  and include even more useful syntax  to parallelize use of the bioinformatics software BLAST across multiple biological input sequences   Here s our example task list   task lst      blast data protein1 faa  blast data protein2 faa    Here s the script we want to run BLAST on a single input file   run blast sh      bin bash blastp  query  1  db  blast db img v400 PROT 00  out  2   outfmt 7  max target seqs 10  num threads  3    Now let s use GNU parallel in the context of a SLURM job script      bin bash  SBATCH  job name job name  SBATCH  account account name  SBATCH  partition partition name  SBATCH  nodes 2  SBATCH  cpus per task 2  SBATCH  time 2 00 00    Command s  to run  example  module load bio blast 2 6 0 module load gnu parallel 2019 03 22  export WDIR your desired path cd  WDIR    set number of jobs based on number of cores available and number of threads per job export JOBS PER NODE   SLURM CPUS ON NODE    SLURM CPUS PER TASK    echo  SLURM JOB NODELIST  sed s n g   hostfile  parallel  jobs  JOBS PER NODE  slf hostfile  wd  WDIR  joblog task log  resume  progress  a task lst sh run blast sh   output blst  SLURM CPUS PER TASK    Some things to notice     Here BLAST will use multiple threads for each job  based on the SLURM CPUS PER TASK variable that is set based on the  c   or  cpus per task  SLURM flag    We programmatically determine how many jobs to run on each node  accounting for the threading    Setting the working directory with  wd  is optional  without that your home directory will be used  if using multiple nodes via  slf  or the current working directory will be used  if using one node    The  resume  and  joblog  flags allow you to easily restart interrupted work without redoing already completed tasks    The  progress  flag causes a progress bar to be displayed    In this case  only one of the three inputs to  run blast sh  is provided in the task list  The second argument is determined from the first  after discarding the path and file extension  and the third is constant across tasks 
---

GNU Parallel is a shell tool for executing jobs in parallel on one or
multiple computers. It's a helpful tool for automating the
parallelization of multiple (often serial) jobs, in particular
allowing one to group jobs into a single SLURM submission to take advantage of the multiple cores on
a given Savio node.
 
A job can be a single core serial task, multi-core
job, or MPI application. A job can also be a command that reads from a
pipe. The typical input is a list of input parameters needed as input
for all the jobs collectively. GNU parallel can then split the input
and pipe it into commands in parallel. GNU parallel makes sure output
from the commands is the same output as you would get had you run the
commands sequentially, and output names can be easily tied to input
file names for simple post-processing. This makes it possible to use
output from GNU parallel as input for other programs.
 
Below we'll show basic usage of GNU parallel and then provide an
extended example illustrating submission of a Savio job that uses GNU
parallel.
 
For full documentation see the [GNU parallel man page](https://www.gnu.org/software/parallel/man.html) and [GNU
parallel tutorial](https://www.gnu.org/software/parallel/parallel_tutorial.html).
 
## Basic usage
 
To motivate usage of GNU parallel, consider how you might automate
running multiple individual tasks using a simple bash for loop. In
this case, our example command involves copying a file. We will copy `file1.in` to `file1.out`, `file2.in` to `file2.out`, etc. 
 
```
for (( i=1; i <= 3; i++ )); do
    cp file${i}.in file${i}.out
done
```
 
 That's fine, but it won't run the tasks in parallel. Let's use GNU
 parallel to do it in parallel:
 
```
parallel -j 2 cp file{}.in file{}.out ::: 1 2 3
ls file*out
# file1.out  file2.out  file3.out
```
 
Based on `-j`, that will use two cores to process the three tasks,
starting the third task when a core becomes free from having finished
either the first or second task. The
`:::` syntax separates the input values: `1 2 3` from the command
being run. Each input value is used in place of `{}` and the `cp`
command is run.
 
### Some bells and whistles
 
We can use multiple inputs per task, distinguishing the inputs by
`{1}`, `{2}`, etc.:
 
```
parallel --link -j 2 cp file{1}.in file{2}.out ::: 1 2 3 ::: 4 5 6
ls file*out
# file4.out  file5.out  file6.out
```
 
Note that `--link` is needed so that 1 is paired with 4, 2 with 5,
etc., instead of doing all possible pairs amongst the two sets. 
 
Of course in many contexts we don't want to have to write out all the
input numbers. We can instead generate them using `seq`:
 
```
parallel -j 2 cp file{}.in file{}.out ::: `seq 3`
```
 
We can use a file containing the list of inputs as a "task list"
instead of using the `:::` syntax. Here we'll also illustrate the special
syntax `{.}` to remove the filename extension.
 
```
parallel -j 2 -a task.lst cp {} {.}.out
```
 
`task.lst` looks like this; it should have the parameter(s) for
separate tasks on separate lines:
 
```
file1.in
file2.in
file3.in
```
 
Next we could use a shell script instead of putting the command
inline:
 
```
parallel -j 2 -a task.lst bash mycp.sh {} {.}.out
# copying file1.in to file1.out
# copying file2.in to file2.out
# copying file3.in to file3.out
```
 
Here's what `mycp.sh` looks like:
 
```
#!/bin/bash 
echo copying ${1} to ${2}
cp ${1} ${2}
```
 
We could also parallelize an arbitrary set of commands rather than
using the same command on a set of arbitrary inputs.
 
```
parallel -j 2 < commands.lst
# hello
# wilkommen
# hola
```
 
Not surprisingly, here's the content of `commands.lst`:
 
```
echo hello
echo wilkommen
echo hola
```
 
Finally, let's see how we would use GNU parallel within the context of
a SLURM batch job.
 
To parallelize on one node, using all the cores on the node:
 
```
module load gnu-parallel/2019.03.22
parallel -j $SLURM_CPUS_ON_NODE < commands.lst
```
 
To parallelize across all the cores on multiple nodes we need to use
the `--slf` flag:
 
```
module load gnu-parallel/2019.03.22
echo $SLURM_JOB_NODELIST |sed s/\,/\\n/g > hostfile
parallel -j $SLURM_CPUS_ON_NODE --slf hostfile < commands.lst
```
 
When using multiple nodes, the working directory will be your home directory, unless you specify otherwise using the `--wd` flag (see below for example usage), and NOT the directory from which `parallel` was called.
 
## Extended example
 
Here we'll put it all together (and include even more useful syntax)
to parallelize use of the bioinformatics software BLAST across
multiple biological input sequences.
 
Here's our example task list, `task.lst`:
 
```
../blast/data/protein1.faa
../blast/data/protein2.faa
<snip>
```
 
Here's the script we want to run BLAST on a single input file,
`run-blast.sh`:
 
```
#!/bin/bash
blastp -query $1 -db ../blast/db/img_v400_PROT.00 -out $2  -outfmt 7 -max_target_seqs 10 -num_threads $3
```
 
Now let's use GNU parallel in the context of a SLURM job script:
 
```
#!/bin/bash
#SBATCH --job-name=job-name
#SBATCH --account=account_name
#SBATCH --partition=partition_name
#SBATCH --nodes=2
#SBATCH --cpus-per-task=2
#SBATCH --time=2:00:00
 
## Command(s) to run (example):
module load bio/blast/2.6.0
module load gnu-parallel/2019.03.22
 
export WDIR=/your/desired/path
cd $WDIR
 
# set number of jobs based on number of cores available and number of threads per job
export JOBS_PER_NODE=$(( $SLURM_CPUS_ON_NODE / $SLURM_CPUS_PER_TASK ))
 
echo $SLURM_JOB_NODELIST |sed s/\,/\\n/g > hostfile
 
parallel --jobs $JOBS_PER_NODE --slf hostfile --wd $WDIR --joblog task.log --resume --progress -a task.lst sh run-blast.sh {} output/{/.}.blst $SLURM_CPUS_PER_TASK
```
 
Some things to notice:
 
 - Here BLAST will use multiple threads for each job, based on the
   SLURM_CPUS_PER_TASK variable that is set based on the `-c` (or
   `--cpus-per-task`) SLURM flag.
 - We programmatically determine how many jobs to run on each node,
   accounting for the threading.
 - Setting the working directory with `--wd` is optional; without that
 your home directory will be used (if using multiple nodes via `--slf`) or the current working directory will be used (if using one node).
 - The `--resume` and `--joblog` flags allow you to easily restart
   interrupted work without redoing already completed tasks.
 - The `--progress` flag causes a progress bar to be displayed.
 - In this case, only one of the three inputs to `run-blast.sh` is provided in the task
   list. The second argument is determined from the first, after
   discarding the path and file extension, and the third is constant
   across tasks.

