---
title: CGRL (Vector/Rosalind) User Guide
keywords: high performance computing, berkeley research computing, cgrl
tags: [hpc, cgrl]
sidebar: hpc_sidebar
permalink: services/high-performance-computing/cgrl-vectorrosalind-user-guide
# DO NOT EDIT THE PREAMBLE BELOW THIS LINE #
search_content: This page provides a high level overview for Computational Genomics Resource Laboratory  CGRL  users  but in most cases we simply link to pages that provide information for both CGRL and non CGRL users  The CGRL provides access to two computing clusters collocated within the larger Savio system administered by Berkeley Research Computing at the University of California  Berkeley  Vector is a heterogeneous cluster that is accessed through the Savio login nodes  but it is independent from the rest of Savio and exclusively used by the CGRL  Rosalind is a condo within Savio  Through the  condo model of access  site baseurl  services high performance computing condos  CGRL users can utilize a number of Savio nodes equal to those contributed by Rosalind    Account requests You can request new accounts through the CGRL by emailing cgrl berkeley edu     Logging in Vector and Rosalind  Savio  use One Time Passwords  OTPs  for login authentication  For details  please see  Logging into BRC Clusters  site baseurl  services high performance computing user guide logging brc clusters  Use SSH to log into   hpc brc berkeley edu    Transferring data To transfer data to from Vector and Rosalind  Savio  please follow  these procedures  site baseurl  services high performance computing user guide transferring data    Storage and backup A variety of  storage systems are available to CGRL users  site baseurl  services high performance computing user guide storing data CGRL storage    Hardware and scheduler configuration Vector and Rosalind are heterogeneous  with a mix of several different types of nodes  Please be aware of these various  hardware configurations  site baseurl  services high performance computing user guide hardware config CGRL hardware  along with their associated  scheduler configurations  site baseurl  services high performance computing user guide running your jobs scheduler config CGRL scheduler  when specifying options for  running your jobs  site baseurl  services high performance computing user guide running your jobs cgrl jobs    Running jobs   The settings for a job in Vector  Note  you don t need to set the  account   partition vector  qos vector batch    The settings for a job in Rosalind  Savio1   partition savio  account co rosalind  qos rosalind savio normal    The settings for a job in Rosalind  Savio2 HTC   partition savio2 htc  account co rosalind  qos rosalind htc2 normal    Low priority jobs Because CGRL is a condo contributor  all CGRL users are entitled to use the extra resources that are available on the SAVIO cluster  across all partitions  through the  low priority QoS  site baseurl  services high performance computing user guide running your jobs low priority    Software configuration Please see our pages on  accessing software already installed  site baseurl  services high performance computing user guide accessing software  and on  installing your own software  site baseurl  services high performance computing user guide installing software  CGRL users have access to the CGRL module farm of bioinformatics software  clusterfs vector home groups software sl 7 x86 64 modfiles  as well as the other module farms on Savio    Getting help For inquiries or service requests regarding the cluster systems  please see  BRC s Getting Help page  site baseurl  services high performance computing getting help  or send email to   For questions about new accounts or installing new biology software please contact the Computational Genomics Resource Laboratory  CGRL  by emailing
---

This page provides a high-level overview for <a href="http://qb3.berkeley.edu/cgrl/" target="_blank">Computational Genomics Resource Laboratory (CGRL)</a> users, but in most cases we simply link to pages that provide information for both CGRL and non-CGRL users.

The CGRL provides access to two computing clusters collocated within the larger Savio system administered by <a href="http://research-it.berkeley.edu/programs/berkeley-research-computing" target="_blank">Berkeley Research Computing</a> at the University of California, Berkeley. Vector is a heterogeneous cluster that is accessed through the Savio login nodes, but it is independent from the rest of Savio and exclusively used by the CGRL. Rosalind is a condo within Savio. Through the [condo model of access]({{ site.baseurl }}/services/high-performance-computing/condos/), CGRL users can utilize a number of Savio nodes equal to those contributed by Rosalind.

#### Account requests

You can request new accounts through the <a href="http://qb3.berkeley.edu/cgrl/" target="_blank">CGRL</a> by emailing <a href="mailto:cgrl@berkeley.edu">cgrl@berkeley.edu</a>.

#### Logging in

Vector and Rosalind (Savio) use One Time Passwords (OTPs) for login authentication. For details, please see [Logging into BRC Clusters]({{ site.baseurl }}/services/high-performance-computing/user-guide/logging-brc-clusters).

Use SSH to log into: `hpc.brc.berkeley.edu`

#### Transferring data

To transfer data to/from Vector and Rosalind (Savio), please
follow [these procedures]({{ site.baseurl }}/services/high-performance-computing/user-guide/transferring-data).

#### Storage and backup

A variety of [storage systems are available to CGRL users]({{ site.baseurl }}/services/high-performance-computing/user-guide/storing-data#CGRL-storage).

#### Hardware and scheduler configuration

Vector and Rosalind are heterogeneous, with a mix of several different types of nodes. Please be aware of these various [hardware configurations]({{ site.baseurl }}/services/high-performance-computing/user-guide/hardware-config/#CGRL-hardware), along with their associated [scheduler configurations]({{ site.baseurl }}/services/high-performance-computing/user-guide/running-your-jobs/scheduler-config#CGRL-scheduler) when specifying options for [running your jobs]({{ site.baseurl }}/services/high-performance-computing/user-guide/running-your-jobs#cgrl-jobs).

#### Running jobs

- The settings for a job in Vector (Note: you don't need to set the "account"): `--partition=vector --qos=vector_batch`  
- The settings for a job in Rosalind (Savio1): `--partition=savio --account=co_rosalind --qos=rosalind_savio_normal`  
- The settings for a job in Rosalind (Savio2 HTC): `--partition=savio2_htc --account=co_rosalind --qos=rosalind_htc2_normal`  


#### Low priority jobs

Because CGRL is a condo contributor, all CGRL users are entitled to use the extra resources that are available on the SAVIO cluster (across all partitions) through the [low priority QoS]({{ site.baseurl }}/services/high-performance-computing/user-guide/running-your-jobs#low-priority).


#### Software configuration

Please see our pages on [accessing software already installed]({{ site.baseurl }}/services/high-performance-computing/user-guide/accessing-software) and on [installing your own software]({{ site.baseurl }}/services/high-performance-computing/user-guide/installing-software). CGRL users have access to the CGRL module farm of bioinformatics software (/clusterfs/vector/home/groups/software/sl-7.x86\_64/modfiles), as well as the other module farms on Savio.

#### Getting help

For inquiries or service requests regarding the cluster systems, please see [BRC's Getting Help page]({{ site.baseurl }}/services/high-performance-computing/getting-help/) or send email to <brc-hpc-help@berkeley.edu>.

For questions about new accounts or installing new biology software please contact the <a href="http://qb3.berkeley.edu/cgrl/" target="_blank">Computational Genomics Resource Laboratory (CGRL)</a> by emailing <cgrl@berkeley.edu>
